# Imagen Raspberry - ROS

En el siguiente archivo encontrarán una copia de la imagen para la Raspberry Pi 4. Esta imagen contiene el sistema operativo Raspian y ya cuenta con ROS instalado. 
Para poder utilizarla tienen que volverla booteable en a través de un programa como Rufus o Banela Exchec en la tarjeta micro SD que cuentan.
